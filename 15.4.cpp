// 15.4.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

void oddoreven(const int N, bool showeven)
{
    
    if (showeven == true)
    {
        std::cout << "Even numbers from " << 0 << " to " << N << "\n";
        for (int i = 0; i <= N; i++)
        {
            if (i % 2 == 0)
            {
                std::cout << i << "\n";
            }
        }

    }
    else
    {
        std::cout << "Odd numbers from " << 0 << " to " << N << "\n";
        for (int i = 0; i <= N; i++)
        {
            if (i % 2 != 0)
            {
                std::cout << i << "\n";
            }
        }
    }
}

int main()
{
    const int N = 19;
    std::cout << "Even numbers from " << 0 << " to " << N << "\n";
    for(int i = 0; i <= N; i++)
    {
        if (i % 2 == 0)
        {
            std::cout << i << "\n";
        }
    }
    std::cout << "Start function oddoreven\n";
    // �����, �� �������� ����� ���� ������������. true - ������, false - ��������.
    oddoreven(15, true); 
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
